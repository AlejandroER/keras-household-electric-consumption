require(lubridate)
require(reticulate)

# Read the Single household consumption time series and use the Date and Time
#columns to create a datetime POSIXct column for ordering.
Data <- read.csv("household_power_consumption.txt",sep = ";",na.strings = "?")
Data$DateTime <- as_datetime(paste(Data$Date,Data$Time),format = "%d/%m/%Y %H:%M:%S")
# Set datetime as 1st column.
Data <- Data[,c(10,1:9)]
# Fill in missing data with the data 24H before. This supposes that there is no large 
#variation in consumption between days that are not too far apart.
a <- which(is.na(Data$Global_active_power))
for(i in a){
  Data[i,-(1:3)] <- Data[(i-24*60),-(1:3)]
}
# Add a new entry to the metering category. The remaining power after accounting for 
#kitchen (1), laundry (2) and heating/cooling (3).
Data$Sub_metering_4 <- Data$Global_active_power*1000/60 - (Data$Sub_metering_1+Data$Sub_metering_2+Data$Sub_metering_3)
# # Save the cleaned data to a new csv.
write.table(x = Data,file = "HPC_cleaned.csv",sep = ",",col.names = TRUE,row.names = FALSE)
# Create new dataframe with consumption by day instead of minute.
a <- unique(Data$Date)
DailyData <- lapply(a, function(x){
  Date <- x
  Output <- cbind(Date,as.data.frame(t(colSums(Data[Data$Date %in% Date,4:11]))))
})
DailyData <- do.call(rbind,DailyData)
# # Save the daily consumption data to a new csv.
write.table(x = DailyData,file = "HPC_daily.csv",sep = ",",col.names = TRUE,row.names = FALSE)
# Set Date as datetime variable.
DailyData$Date <- as_datetime(DailyData$Date,format="%d/%m/%Y")
for(i in 1:10) print(weekdays(DailyData$Date[i])) # First Sunday is 2nd entry.
for(i in 1433:1442) print(weekdays(DailyData$Date[i])) # Last Saturday is 1436st entry.
# Separate the data between testing and training sets. Training will be up
#to the last week of 2009 and testing will be the weeks of 2010.
DailyDates <- DailyData[,1]
FirstDay2010 <- as_datetime("01-1-2010",format="%d-%m-%Y")
FirstDay2010 <- which(DailyDates == FirstDay2010)
weekdays(DailyDates[FirstDay2010]) # 2010 starts on a Friday. Test starts on Sunday
# Separate Testing and training datasets.
DailyData_Train <- data.matrix(DailyData[2:(FirstDay2010+1),-1])
DailyData_Test <- data.matrix(DailyData[(FirstDay2010+2):1436,-1])
# Shape the Test set into itś proper form, a (46,7,8) array.
ShapedData_Test <- array(t(DailyData_Test),dim = c(8,7,length(DailyData_Test[,1])/7))
ShapedData_Test <- aperm(ShapedData_Test,c(3,2,1))
# Remove unused variables.
rm(i,a,DailyData,DailyDates,FirstDay2010)
#################################################################################
# Building univariate total_power_consumption dataset for univariate in/out LSTM
#################################################################################
# This function will transform the vector of Global_Active_Power into X and Y
#arrays of input and output respectively. Length_X determines how many days 
#into the past we want to use for predictions. Length_Y determines how many
#days we want to predict into the future.
PrepareUnivarData <- function(DailyData_Train,Length_X,Length_Y=7){
  # Take the first feature of the data.
  UnivarTrain <- DailyData_Train[,1]
  # Create a matrix with Length_X elements in each row. This will be the input.
  X <- t(vapply(1:(length(UnivarTrain)-Length_Y-Length_X), function(x){
    return(c(UnivarTrain[x:(x+Length_X-1)]))
  }, FUN.VALUE = rep.int(1,Length_X)))
  # Create a matrix with Length_Y elements in each row. This will be the output.
  Y <- t(vapply((Length_X+1):(length(UnivarTrain)-Length_Y), function(x){
    return(c(UnivarTrain[x:(x+Length_Y-1)]))
  }, FUN.VALUE = rep.int(1,Length_Y)))
  return(list(X=array(X,dim = c(dim(X),1)),Y=array(Y,dim = c(dim(Y),1))))
}


#################################################################################
# Building multivariate dataset for univariate total_power_consumption LSTM
#################################################################################
# This function will transform the array of power consumption into X and Y
#arrays of input and output respectively. Length_X determines how many days 
#into the past we want to use for predictions. Length_Y determines how many
#days we want to predict into the future. It will contain all features 
#present in the original data.
PrepareMultivarData <- function(DailyData_Train,Length_X,Length_Y=7){
  require(abind)
  # Create a list with each entry being a different sample with dimensions
  #(Length_X,8)
  X <- lapply(1:(dim(DailyData_Train)[1]-Length_X-Length_Y), function(x){
    return(DailyData_Train[x:(x+Length_X-1),])
  })
  # Bind all elements of list X into an array with dimensions (length(X),Length_X,8)
  X <- abind(X,along = 1/2)
  # # Create a list with each entry being a different sample with dimensions
  #(Length_Y,8)
  Y <- lapply((Length_X+1):(dim(DailyData_Train)[1]-Length_Y), function(x){
    return(array(DailyData_Train[x:(x+Length_Y-1),1],dim = c(Length_Y,1)))
  })
  # Bind all elements of list Y into an array with dimensions (length(Y),Length_Y,1)
  Y <- abind(Y,along=1/2)
  # Return both arrays X and Y as elements of a list.
  return(list(X=X,Y=Y))
}
